
# Camera
PRODUCT_BROKEN_VERIFY_USES_LIBRARIES := true
PRODUCT_PACKAGES += \
 #   GoogleCamera

# Dolby
#BOARD_VENDOR_SEPOLICY_DIRS += \
 #   vendor/google/pixelparts/sepolicy/dolby

# Parts
PRODUCT_PACKAGES += \
    GoogleParts \
    PixelFrameworksOverlay \
    TurboAdapter
